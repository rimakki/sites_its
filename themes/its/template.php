<?php

/**
 * @file
 * This file provides theme override functions for the current theme.
 */

/**
 * Implements hook_template_preprocess_node().
 *
 * Preprocess variables for node.tpl.php.
 */
function its_preprocess_node(&$variables) {

  // Customize display of $submitted to only show authors to authorized users,
  // and to include taxonomy terms, rather than displaying those separately.

  if (!empty($variables['submitted'])) {
    $submitted = t('Posted !created', array('!created' => format_date($variables['created'], 'custom', 'l, F j, Y \a\t h:ia')));
    if(user_access('access private content')){
      $submitted .= t(' by !name', array('!name' => $variables['name']));
    }
    if (!empty($variables['field_categories']) && $variables['page']) {
      $categories = array();
      foreach (element_children($variables['field_categories']) as $term) {
        $categories[] = l($variables['content']['field_categories'][$term]['#title'], $variables['content']['field_categories'][$term]['#href']);
      }
      $submitted .= t(' in !terms', array('!terms' => implode(', ', $categories)));
      unset($variables['field_categories']);
      unset($variables['content']['field_categories']);
    }

    $variables['submitted'] = $submitted;
  }

  // Get rid of the private icon in links and set a node class instead.
  if (!empty($variables['content']['links']['private'])) {
    unset ($variables['content']['links']['private']);
    $variables['classes_array'][] = 'node-private';
  }

}

